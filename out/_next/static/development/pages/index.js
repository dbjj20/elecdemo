(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["static/development/pages/index.js"],{

/***/ "./node_modules/next/dist/build/webpack/loaders/next-client-pages-loader.js?page=%2F&absolutePagePath=%2FUsers%2Fxdstriker%2Fprivdir%2Felectron%2Fdemo%2Fpages%2Findex.js&hotRouterUpdates=true!./":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/next/dist/build/webpack/loaders/next-client-pages-loader.js?page=%2F&absolutePagePath=%2FUsers%2Fxdstriker%2Fprivdir%2Felectron%2Fdemo%2Fpages%2Findex.js&hotRouterUpdates=true ***!
  \******************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


    (window.__NEXT_P = window.__NEXT_P || []).push([
      "/",
      function () {
        var mod = __webpack_require__(/*! ./pages/index.js */ "./pages/index.js");
        if (true) {
          module.hot.accept(/*! ./pages/index.js */ "./pages/index.js", function () {
            if (!next.router.components["/"]) return;
            var updatedPage = __webpack_require__(/*! ./pages/index.js */ "./pages/index.js");
            next.router.update("/", updatedPage);
          });
        }
        return mod;
      }
    ]);
  

/***/ }),

/***/ "./node_modules/react/index.js":
/*!*******************************************************************************************!*\
  !*** delegated ./node_modules/react/index.js from dll-reference dll_2adc2403d89adc16ead0 ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(/*! dll-reference dll_2adc2403d89adc16ead0 */ "dll-reference dll_2adc2403d89adc16ead0"))("./node_modules/react/index.js");

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function HomePage() {
  return __jsx("div", null, __jsx("section", {
    "class": "hero is-primary"
  }, __jsx("div", {
    "class": "hero-body"
  }, __jsx("div", {
    "class": "container"
  }, __jsx("h1", {
    "class": "title"
  }, "Primary title"), __jsx("h2", {
    "class": "subtitle"
  }, "Primary subtitle")))), __jsx("div", {
    className: "container is-fullhd"
  }, __jsx("div", {
    className: "notification"
  }, "Vista con contenedor ", __jsx("strong", null, "fullwidth"), " ", __jsx("em", null, "until"), " the ", __jsx("code", null, "$fullhd"), " breakpoint.", __jsx("div", {
    className: "card "
  }, __jsx("div", {
    className: "card-image"
  }, __jsx("figure", {
    className: "image is-128x128"
  }, __jsx("img", {
    src: "https://bulma.io/images/placeholders/256x256.png",
    alt: "Placeholder image"
  }))), __jsx("div", {
    className: "card-content"
  }, __jsx("div", {
    className: "media"
  }, __jsx("div", {
    className: "media-left"
  }, __jsx("figure", {
    className: "image is-48x48"
  }, __jsx("img", {
    src: "https://bulma.io/images/placeholders/96x96.png",
    alt: "Placeholder image"
  }))), __jsx("div", {
    className: "media-content"
  }, __jsx("p", {
    className: "title is-4"
  }, "John Smith"), __jsx("p", {
    className: "subtitle is-6"
  }, "@johnsmith"))), __jsx("div", {
    className: "content"
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris. ", __jsx("a", null, "@bulmaio"), ".", __jsx("a", {
    href: "#"
  }, "#css"), " ", __jsx("a", {
    href: "#"
  }, "#responsive"), __jsx("br", null)), __jsx("footer", {
    className: "card-footer"
  }, __jsx("a", {
    className: "card-footer-item",
    onClick: function onClick() {
      return window.print();
    }
  }, "Print"), __jsx("a", {
    className: "card-footer-item"
  }, "Edit"), __jsx("a", {
    className: "card-footer-item"
  }, "Delete")))))), __jsx("footer", {
    "class": "footer"
  }, __jsx("div", {
    "class": "content has-text-centered"
  }, __jsx("p", null, __jsx("strong", null, "Bulma"), " by ", __jsx("a", {
    href: "https://jgthms.com"
  }, "Dylan"), ". The source code is licensed", __jsx("a", {
    href: "http://opensource.org/licenses/mit-license.php"
  }, "MIT"), ". The website content is licensed ", __jsx("a", {
    href: "http://creativecommons.org/licenses/by-nc-sa/4.0/"
  }, "CC BY NC SA 4.0"), "."))));
}

/* harmony default export */ __webpack_exports__["default"] = (HomePage);

/***/ }),

/***/ 1:
/*!**********************************************************************************************************************************************************!*\
  !*** multi next-client-pages-loader?page=%2F&absolutePagePath=%2FUsers%2Fxdstriker%2Fprivdir%2Felectron%2Fdemo%2Fpages%2Findex.js&hotRouterUpdates=true ***!
  \**********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! next-client-pages-loader?page=%2F&absolutePagePath=%2FUsers%2Fxdstriker%2Fprivdir%2Felectron%2Fdemo%2Fpages%2Findex.js&hotRouterUpdates=true! */"./node_modules/next/dist/build/webpack/loaders/next-client-pages-loader.js?page=%2F&absolutePagePath=%2FUsers%2Fxdstriker%2Fprivdir%2Felectron%2Fdemo%2Fpages%2Findex.js&hotRouterUpdates=true!./");


/***/ }),

/***/ "dll-reference dll_2adc2403d89adc16ead0":
/*!*******************************************!*\
  !*** external "dll_2adc2403d89adc16ead0" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = dll_2adc2403d89adc16ead0;

/***/ })

},[[1,"static/runtime/webpack.js"]]]);
//# sourceMappingURL=index.js.map