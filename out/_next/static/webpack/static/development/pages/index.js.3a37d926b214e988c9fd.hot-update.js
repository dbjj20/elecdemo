webpackHotUpdate("static/development/pages/index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function HomePage() {
  return __jsx("div", null, __jsx("section", {
    "class": "hero is-primary"
  }, __jsx("div", {
    "class": "hero-body"
  }, __jsx("div", {
    "class": "container"
  }, __jsx("h1", {
    "class": "title"
  }, "Primary title"), __jsx("h2", {
    "class": "subtitle"
  }, "Primary subtitle")))), __jsx("div", {
    className: "container is-fullhd"
  }, __jsx("div", {
    className: "notification"
  }, "Vista con contenedor ", __jsx("strong", null, "fullwidth"), " ", __jsx("em", null, "until"), " the ", __jsx("code", null, "$fullhd"), " breakpoint.", __jsx("div", {
    className: "card "
  }, __jsx("div", {
    className: "card-image"
  }, __jsx("figure", {
    className: "image is-4by3"
  }, __jsx("img", {
    src: "https://bulma.io/images/placeholders/1280x960.png",
    alt: "Placeholder image"
  }))), __jsx("div", {
    className: "card-content"
  }, __jsx("div", {
    className: "media"
  }, __jsx("div", {
    className: "media-left"
  }, __jsx("figure", {
    className: "image is-48x48"
  }, __jsx("img", {
    src: "https://bulma.io/images/placeholders/96x96.png",
    alt: "Placeholder image"
  }))), __jsx("div", {
    className: "media-content"
  }, __jsx("p", {
    className: "title is-4"
  }, "John Smith"), __jsx("p", {
    className: "subtitle is-6"
  }, "@johnsmith"))), __jsx("div", {
    className: "content"
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris. ", __jsx("a", null, "@bulmaio"), ".", __jsx("a", {
    href: "#"
  }, "#css"), " ", __jsx("a", {
    href: "#"
  }, "#responsive"), __jsx("br", null)), __jsx("footer", {
    className: "card-footer"
  }, __jsx("a", {
    className: "card-footer-item",
    onClick: function onClick() {
      return window.print();
    }
  }, "Print"), __jsx("a", {
    className: "card-footer-item"
  }, "Edit"), __jsx("a", {
    className: "card-footer-item"
  }, "Delete")))))), __jsx("footer", {
    "class": "footer"
  }, __jsx("div", {
    "class": "content has-text-centered"
  }, __jsx("p", null, __jsx("strong", null, "Bulma"), " by ", __jsx("a", {
    href: "https://jgthms.com"
  }, "Jeremy Thomas"), ". The source code is licensed", __jsx("a", {
    href: "http://opensource.org/licenses/mit-license.php"
  }, "MIT"), ". The website content is licensed ", __jsx("a", {
    href: "http://creativecommons.org/licenses/by-nc-sa/4.0/"
  }, "CC BY NC SA 4.0"), "."))));
}

/* harmony default export */ __webpack_exports__["default"] = (HomePage);

/***/ })

})
//# sourceMappingURL=index.js.3a37d926b214e988c9fd.hot-update.js.map