
function HomePage() {
  return (
    <div>
    <section className="hero is-primary">
      <div className="hero-body">
        <div className="container">
          <h1 className="title">
            Primary title
          </h1>
          <h2 className="subtitle">
            Primary subtitle
          </h2>
        </div>
      </div>
    </section>
    <div className="container is-fullhd">
      <div className="notification">
        Vista con contenedor <strong>fullwidth</strong> <em>until</em> the <code>$fullhd</code> breakpoint.
        <div className="card ">
          <div className="card-image">
            <figure className="image is-128x128">
              <img src="https://bulma.io/images/placeholders/256x256.png" alt="Placeholder image"/>
            </figure>
          </div>
          <div className="card-content">
            <div className="media">
              <div className="media-left">
                <figure className="image is-48x48">
                  <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image"/>
                </figure>
              </div>
              <div className="media-content">
                <p className="title is-4">John Smith</p>
                <p className="subtitle is-6">@johnsmith</p>
              </div>
            </div>

            <div className="content">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Phasellus nec iaculis mauris. <a>@bulmaio</a>.
              <a href="#">#css</a> <a href="#">#responsive</a>
              <br/>
            </div>
            <footer className="card-footer">
              <a className="card-footer-item" onClick={() => window.print()}>Print</a>
              <a className="card-footer-item">Edit</a>
              <a className="card-footer-item">Delete</a>
            </footer>
          </div>
        </div>
      </div>
    </div>
    <footer className="footer">
      <div className="content has-text-centered">
        <p>
          <strong>Bulma</strong> by <a href="https://jgthms.com">Dylan</a>. The source code is licensed
          <a href="http://opensource.org/licenses/mit-license.php">MIT</a>. The website content
          is licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY NC SA 4.0</a>.
        </p>
      </div>
    </footer>
    </div>
  )
}

export default HomePage
